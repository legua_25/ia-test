﻿using System;
using UnityEngine;
using Chicken.Invasion;

namespace Chicken.Invasion {
	
	public abstract class Entity : AbstractEntity {

		public StateMachine state_manager;

		public StateMachine StateManager { get { return this.state_manager; } }

		protected override void Awake() {

			base.Awake();

			if (this.IsManaged && this.state_manager == null) {

				Debug.Log("A \"StateMachine\" is required to run a managed entity.");
				throw new Exception("StateMachine instance missing.");
			}
		}
		protected override void Update() {
			base.Update();
		}

	}

	public delegate void Strategy(AbstractEntity e);

}

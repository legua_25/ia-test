﻿using UnityEngine;
using System.Collections.Generic;

namespace Chicken.Invasion {

	public class StateMachine : MonoBehaviour {

		public AbstractEntity subject;
		private Stack<Strategy> stack;

		public Strategy Current { get { return (this.stack.Count > 0 ? this.stack.Peek() : null); } }

		protected void Awake() {
			this.stack = new Stack<Strategy>();
		}
		protected void Update() {

			Strategy s = (this.stack.Count > 0 ? this.stack.Peek() : null);
			if (s != null)
				s(this.subject);
		}

		public Strategy Pop() {
			return this.stack.Pop();
		}
		public void Push(Strategy s) {

			if (this.stack.Count == 0 || this.stack.Peek() != s)
				this.stack.Push(s);
		}

	}

}

﻿using UnityEngine;
using System.Collections;
using Chicken.Invasion;

namespace Chicken.Invasion {
	
	public class Chicken : Entity {

		public float wander_radius;
		public float wander_time_limit;
		private Vector3 wander_position;
		private float wander_time;

		public float range_of_view;
		public float attack_range;
		public float leap_height;
		public float cooldown_time;
		private float cooldown;

		protected override void Start() {

			base.Start();

			if (this.IsManaged) {

				Debug.Log("Entity #" + this.ID + ": pushed state Wander.");
				this.StateManager.Push(this.Wander);
			}
		}
		private void Wander(AbstractEntity e) {

			this.cooldown = this.cooldown_time;
			GameObject player_go = GameObject.FindGameObjectWithTag("Player") as GameObject;
			Entity player = (player_go != null ? player_go.GetComponent<Entity>() : null);

			if (player != null) {
				
				Vector3 player_pos = player.transform.position, 
				e_pos = e.transform.position;
				
				if (Vector3.Distance(player_pos, e_pos) < (this.range_of_view * 2.0f)) {

					Debug.Log("Entity #" + this.ID + ": pushed state Follow.");
					this.StateManager.Push(this.Follow);
				}
				else {
					
					if ((this.wander_time += Time.deltaTime) > this.wander_time_limit) {
						
						this.wander_time = 0;
						Vector3 position = e.transform.position, random = (Random.insideUnitSphere * this.wander_radius);
						position.x = (position.x * random.x);
						position.z = (position.z * random.z);
						position.y = 1;
						
						this.wander_position = position;
					}
					
					e.transform.LookAt(this.wander_position);
					e.transform.position = Vector3.MoveTowards(e.transform.position, this.wander_position, 0.1f);
				}
			}
		}
		private void Follow(AbstractEntity e) {

			GameObject player_go = GameObject.FindGameObjectWithTag("Player") as GameObject;
			Entity player = (player_go != null ? player_go.GetComponent<Entity>() : null);

			if (player != null) {
				
				Vector3 player_pos = player.transform.position, 
				e_pos = e.transform.position;
				
				if (Vector3.Distance(player_pos, e_pos) >= (this.range_of_view * 2.0f)) {

					Debug.Log("Entity #" + this.ID + ": restored state Wander.");
					this.StateManager.Pop();
				}
				else if (Vector3.Distance(player_pos, e_pos) < (this.attack_range * 2.0f)) {

					if ((this.cooldown += Time.deltaTime) >= this.cooldown_time) {

						Debug.Log("Entity #" + this.ID + ": pushed state Attack.");
						this.StateManager.Push(this.Attack);
					}
				}
				else {

					Vector3 position = player_pos;
					position.y = 1;
					
					e.transform.LookAt(position);
					position = Vector3.MoveTowards(e.transform.position, position, (0.1f * e.Stats.Speed));
					position.y = player_pos.y;
					e.transform.position = position;
				}
			}
			else {

				Debug.Log("Entity #" + this.ID + ": restored state Wander.");
				this.StateManager.Pop();
			}
		}
		private void Attack(AbstractEntity e) {

			GameObject player_go = GameObject.FindGameObjectWithTag("Player") as GameObject;
			Entity player = (player_go != null ? player_go.GetComponent<Entity>() : null);
			
			if (player != null) {
				
				Vector3 player_pos = player.transform.position, 
				e_pos = e.transform.position;
				
				 if (Vector3.Distance(player_pos, e_pos) >= (this.attack_range * 2.0f)) {
					
					Debug.Log("Entity #" + this.ID + ": restored state Follow.");
					this.StateManager.Pop();
				}
				else {

					Vector3 position = (Vector3.up * this.leap_height), 
					        leap_position = (player_pos + position);

					e.transform.position = Vector3.MoveTowards(leap_position, position, 0.1f);
					this.cooldown = 0.0f;

					Debug.Log("Entity #" + this.ID + ": restored state Follow.");
					this.StateManager.Pop();
				}
			}
			else {
				
				Debug.Log("Entity #" + this.ID + ": restored state Follow.");
				this.StateManager.Pop();
			}
		}
		private IEnumerator Wait() {
			yield return new WaitForSeconds(1.0f * this.cooldown_time);
		}

	}
	
}

﻿using UnityEngine;
using System.Collections;

namespace Chicken.Invasion {

	public sealed class Attacker : MonoBehaviour {

		public Entity subject;
		public bool active;

		public bool Active {

			get { return this.active; }
			set { this.active = value; }
		}

		protected void OnTriggerEnter(Collider other) {

			if (this.active) {

				if (other.gameObject.GetComponent<Entity>() != null) {

					Entity e = other.gameObject.GetComponent<Entity>();
					if (e.IsManaged && this.Active)
						e.Stats.Health -= this.subject.Stats.Attack;

					this.active = false;
				}
			}
		}

	}

}
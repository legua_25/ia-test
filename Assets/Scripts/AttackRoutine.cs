﻿using UnityEngine;
using System;
using System.Collections;

namespace Chicken.Invasion {

	public sealed class AttackRoutine : MonoBehaviour {

		public float cooldown;
		public float attack_speed;
		public Attacker collider_bounds;
		private float current_cooldown;

		protected void Awake() {

			if (this.collider_bounds == null) {
				
				Debug.Log("Subject must be assigned for the script to work.");
				throw new Exception("Subject is not set.");
			}

			if (this.cooldown < 1.0f)
				this.cooldown = 1.0f;
			else if (this.attack_speed <= 0.0f)
				this.attack_speed = 1.0f;

			this.current_cooldown = this.cooldown;
		}
		protected void Update() {

			this.current_cooldown += Time.deltaTime;

			if (Input.GetButtonDown("Fire1")) {
				if (this.current_cooldown > this.cooldown) {

					Debug.Log("Entity performing attack routine.");
					this.collider_bounds.Active = true;
				}
			}
		}
	}

}

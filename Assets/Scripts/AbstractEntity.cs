﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Chicken.Invasion;

namespace Chicken.Invasion {
	
	public abstract class AbstractEntity : MonoBehaviour {
		
		public Stats stats;
		private int id;
		public bool managed = true;
		
		public Stats Stats { get { return this.stats; } }
		public int ID { get { return this.id; } }
		public bool IsManaged { get { return this.managed; } }
		
		protected virtual void Awake() {

			if (this.stats == null) {
				
				this.stats = this.gameObject.GetComponent<Stats>();
				if (this.stats == null) {
					
					Debug.Log("Invalid usage of entity: a component of type \"Stats\" must always be associated.");
					throw new Exception("Invalid usage of entity.");
				}
			}

			this.id = new System.Random(DateTime.Now.Millisecond).Next(1, short.MaxValue);
		}
		protected virtual void Start() { ; }
		protected virtual void Update() {

			if (this.Stats.Health <= 0) {

				// Score.Value += this.Stats.Cost;
				GameObject.Destroy(this.gameObject);
			}
		}
		
	}

}

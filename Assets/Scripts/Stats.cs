﻿using UnityEngine;
using System;

namespace Chicken.Invasion {

	public sealed class Stats : MonoBehaviour {

		public int health = 1;
		public int attack = 1;
		public int speed = 1;
		public int cost = 100;

		public int Health {

			get { return this.health; }
			set { this.health = Math.Min(0, value); }
		}
		public int Attack { get { return this.attack; } }
		public int Speed { get { return this.speed; } }
		public int Cost { get { return this.cost; } }

	}

}

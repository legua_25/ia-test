﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Chicken.Invasion {

	public sealed class Spawner : MonoBehaviour {

		public int creation_cap;
		public Entity spawned;
		public int min_spawn_time;
		public int max_spawn_time;

		protected void Awake() {

			if (this.spawned == null) {

				Debug.Log("Cannot create unassigned reference. Set spawned entity first.");
				throw new Exception("Cannot create unassigned reference.");
			}

			if (this.min_spawn_time < 1)
				this.min_spawn_time = 1;
			if (this.max_spawn_time < 2)
				this.max_spawn_time = 2;

			if (this.min_spawn_time == this.max_spawn_time) {

				this.min_spawn_time = 1;
				this.max_spawn_time = 2;
			}
		}

		protected void Update() {

			List<Entity> entities = new List<Entity>(GameObject.FindObjectsOfType<Entity>());
			List<Entity> active = entities.FindAll(delegate (Entity e) { return (e.IsManaged); });

			if (active.Count < this.creation_cap) {

				float time = (new System.Random(DateTime.Now.Millisecond).Next(this.min_spawn_time, this.max_spawn_time) + UnityEngine.Random.value);
				this.StartCoroutine(this.Wait(time));

				Vector3 create_position = (this.transform.position + UnityEngine.Random.insideUnitSphere);
				create_position.y = 1;
				create_position.x *= this.transform.localScale.x;
				create_position.z *= this.transform.localScale.z;

				GameObject enemy = GameObject.Instantiate(this.spawned, create_position, new Quaternion()) as GameObject;
			}
		}
		private IEnumerator Wait(float time) {
			yield return new WaitForSeconds(time);
		}

	}

}